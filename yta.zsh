#!/bin/zsh
 
# Download given URL as mp3 via youtube-dl into ~/Downloads
 
yt-dlp -i -c -o "~/Downloads/%(title)s-%(id)s.%(ext)s" --extract-audio --audio-format mp3 --audio-quality 0 "$1"
