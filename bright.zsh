#!/usr/bin/zsh

source settings_bright-dark.zsh

# Get the ID of the Gnome Terminal profile defined in GNOME_TERM_BRIGHT
# via https://askubuntu.com/questions/1068595/edit-gnome-terminal-profile-from-command-line-on-18-04
SCHEME_ID=`dconf dump /org/gnome/terminal/legacy/profiles:/ | egrep '([:.*]|visible-name)' | tr -d "][\'" | tr ':' '=' | cut -d'=' -f2 | paste - - | grep "$GNOME_TERM_BRIGHT\$" | cut -f1`

if [ -z "${SCHEME_ID}" ]; then
	echo "Please modify the GNOM_TERM_* variables in settings_bright-dark.zsh to match the profile names of your Gnome Shell!"
	exit 1;
fi

# TODO:
# Change profile of all open Gnome Termina session via a dbus event ... Don't know how yet
#
#signal time=1560093741.531463 sender=:1.211 -> destination=(null destination) serial=1330 path=/org/gnome/Terminal/window/15; interface=org.gtk.Actions; member=Changed
#   array [
#   ]
#   array [
#   ]
#   array [
#      dict entry(
#         string "profile"
#         variant             string "9c290444-da3f-418b-bda1-1d84eab281ae"
#      )
#   ]
#   array [
#   ]
#
# d-feet
# https://askubuntu.com/questions/1068595/edit-gnome-terminal-profile-from-command-line-on-18-04
# https://wiki.gnome.org/Apps/Terminal/FAQ#How_can_I_change_a_profile_setting_from_the_command_line.3F
# https://stackoverflow.com/questions/660442/switch-gnome-terminal-profile-from-the-command-line
# 

# http://www.kaizou.org/2014/06/dbus-command-line/
#dbus-send --session           \
#  --dest=org.gnome.Terminal \
#  --type=method_call          \
#  --print-reply               \
#  /org/gnome/Terminal       \
#  org.freedesktop.DBus.Introspectable.Introspect >gnome.Termina.interfaces.txt


# WORK CODE. DO NOT CHANGE.

# Change (g)vim coloscheme
grep -q -i "colorscheme $VIMBRIGHT"  ~/.vimrc
if [ $? -ne 0 ]; then
	echo "Set (g)vim colorscheme '$VIMBRIGHT' ..."
	sed -i "/colorscheme/s/colorscheme .*/colorscheme $VIMBRIGHT/" ~/.vimrc
else
	echo "(gvim) colorscheme alread set to '$VIMBRIGHT' ..."
fi

# Change TaskWarrior and TaskShell Colour Scheme
# Sledgehammer approach - assume there is only one include line. 
# Crude, but it works
sed -i "s#^include .*#include $TWTHEMEDIR/$TWLIGHT#" ~/.taskrc
echo "TaskWarrior theme changed from '$TWDARK' to '$TWLIGHT' ..."

# change current terminal profile
# xdotool is a patch, but there is no way in gnome terminal to switch profiles
# from the command line
#echo "key Alt+t p $SELECTPROFILE Return" | xdotool -
# echo "Terminal profile set"

# Set default terminal profile
gsettings set org.gnome.Terminal.ProfilesList default "$SCHEME_ID"
echo "Default Gnome Termina profile set to $SCHEME_ID<"

# Change TMUX Colours
sed -i "/@colors-solarized/s/$TMUXDARK/$TMUXLIGHT/" ~/.tmux.conf
echo "tmux colours changed from '$TMUXDARK' to '$TMUXLIGHT' ..."
echo "\tIf tmux is already running: Strg-Y I to reload."

# Change zsh dircolors
source ~/.zshrc && setupsolarized "$ZSH_DIRCOLOR_LIGHT"
echo "zsh directory colours changed to '$ZSH_DIRCOLOR_LIGHT' ..."
echo "\tIf zsh is already running: 'omz reload' to reload."

echo ""
echo $0 "all set!"
