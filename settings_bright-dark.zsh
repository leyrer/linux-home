#!/usr/bin/zsh

# SETTINGS. DO CHANGE

# Bright colour scheme for (g)Vim
VIMBRIGHT='morning'
VIMDARK='solarized'

# Task Warrior
TWTHEMEDIR=/usr/share/taskwarrior
TWDARK="solarized-dark-256.theme"
# light-256 is easier to read then solarized-light-256
TWLIGHT="light-256.theme"

# Tmux
TMUXDARK="256"
TMUXLIGHT="light"

#Gnome Terminal
GNOME_TERM_BRIGHT='Solarized Light'
GNOME_TERM_DARK='Solarized Dark alternative'

#ZSH dircolors
ZSH_DIRCOLOR_LIGHT='dircolors.ansi-light'
ZSH_DIRCOLOR_DARK='dircolors.256dark'



