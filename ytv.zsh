#!/bin/zsh
 
# the following command will typically select the actual best single file video quality resolution instead of video quality bit-rate:

yt-dlp "$1"
