#!/bin/sh

PRJ_LIST_WIDTH=25
TS='2DO'
WIN='TaskWarrior'
PP="1"
TP="2"
CP="3"

c=`tput cols`
w=$(($c-$PRJ_LIST_WIDTH))

# -d/detach so I can send commands and set up the whole thing
tmux new-session -d -s "$TS" -n "$WIN" "watch -tc task proj"

# ‘mysession:mywindow.1’

tmux split-window -t "$TS:$WIN.$PP" -h -l 100 'tasksh'
tmux split-window -t "$TS:$WIN.$TP" -v '/bin/bash'

#Dirty hack to resize ProjectPane
tmux send-keys -t "$TS:$WIN.$CP" "tmux resize-pane -t "$TS:$WIN.$PP" -x $PRJ_LIST_WIDTH" C-m
tmux send-keys -t "$TS:$WIN.$CP" "tmux kill-pane -t \"$TS:$WIN.$CP\"" C-m

tmux send-keys -t 'tasks:TaskWarrior.1' "sync" C-m
tmux send-keys -t 1.right "context Privat" C-m
tmux send-keys -t 1.right "next" C-m

#(re)attach to current terminal
tmux -2 attach-session -d 
