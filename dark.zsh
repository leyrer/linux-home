#!/usr/bin/zsh

source settings_bright-dark.zsh

# Get the ID of the Gnome Terminal profile defined in GNOME_TERM_BRIGHT
# via https://askubuntu.com/questions/1068595/edit-gnome-terminal-profile-from-command-line-on-18-04
SCHEME_ID=`dconf dump /org/gnome/terminal/legacy/profiles:/ | egrep '([:.*]|visible-name)' | tr -d "][\'" | tr ':' '=' | cut -d'=' -f2 | paste - - | grep "$GNOME_TERM_DARK\$" | cut -f1`

if [ -z "${SCHEME_ID}" ]; then
	echo "Please modify the GNOM_TERM_* variables in settings_bright-dark.zsh to match the profile names of your Gnome Shell!"
	exit 1;
fi

# Change (g)vim coloscheme
grep -q -i "colorscheme $VIMDARK"  ~/.vimrc
if [ $? -ne 0 ]; then
	echo "Set (g)vim colorscheme '$VIMDARK' ..."
	sed -i "/colorscheme/s/colorscheme .*/colorscheme $VIMDARK/" ~/.vimrc
else
	echo "(gvim) colorscheme alread set to '$VIMDARK' ..."
fi

# Change TaskWarrior and TashShell Colour Scheme
# Sledgehammer approach - assume there is only one include line. 
# Crude, but it works
sed -i "s#^include .*#include $TWTHEMEDIR/$TWDARK#" ~/.taskrc
echo "TaskWarrior theme changed from '$TWLIGHT' to '$TWDARK' ..."

# change current terminal profile to solarized dark
# xdotool is a patch, but there is no way in gnome terminal to switch profiles
# from the command line
#echo "key Alt+t p $SELECTPROFILE Return" | xdotool -
#echo "Terminal profile set"

# Set default terminal profile
gsettings set org.gnome.Terminal.ProfilesList default "$SCHEME_ID"
echo "Default Gnome Termina profile set"

# Change TMUX Colours
sed -i "/@colors-solarized/s/$TMUXLIGHT/$TMUXDARK/" ~/.tmux.conf
echo "tmux colours changed from '$TMUXDARK' to '$TMUXLIGHT' ..."
echo "\tIf tmux is already running: Strg-Y I to reload"

# Change zsh dircolors
source ~/.zshrc && setupsolarized "$ZSH_DIRCOLOR_DARK"
echo "zsh directory colours changed to '$ZSH_DIRCOLOR_DARK' ..."
echo "\tIf zsh is already running: 'omz reload' to reload."

echo ""
echo $0 " all set!"
