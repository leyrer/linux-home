#!/usr/bin/env zsh
# 
# Edit my personal qutes collectiona and regenerate the corresponding
# fortunes file afterwards.
#
# Martin Leyrer, martin@leyrer.priv.at
#
set -eu -o pipefail

QUOTEFILE="/home/leyrer/Documents/signatures/sources/nice-quotes.txt"
FORTUNEFILE="/home/leyrer/Documents/signatures/sources/nice-quotes.txt.dat"

#-f        GUI: Do not disconnect from the program that started Vim.
#          'f' stands for "foreground".  If omitted, the GUI forks a new
#          process and exits the current one.  "-f" should be used when
#          gvim is started by a program that will wait for the edit
#          session to finish (e.g., mail or readnews).  If you want gvim
#          never to fork, include 'f' in 'guioptions' in your |gvimrc|.

gvim -f "$QUOTEFILE" 
strfile "$QUOTEFILE" "$FORTUNEFILE"

